// Soal Luas Lingkaran
function luasLingkaran(r){
	var luas = 22/7*r*r
	return luas
}
luasLingkaran = luasLingkaran(10)
console.log(luasLingkaran)

// Soal Luas Segitiga
function luasSegitiga(a,t){
	var luas = a*t/2
	return luas
}
luasSegitiga = luasSegitiga(4,10);
console.log(luasSegitiga);

// Soal Luas Persegi
function luasPersegi(s){
	var luas = s*s
	return luas
}
luasPersegi = luasPersegi(10)
console.log(luasPersegi)


// Soal no 13
var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"]
var sortAlatTulis = daftarAlatTulis.sort()
var a = 0
while(a<sortAlatTulis.length){
	console.log(sortAlatTulis[a])
	a++
}

/*// Soal no 14 Tampilkan bingtang
function tampilkanBintang(jumlahBintang){
	var a = "";
	for(i=1; i<=jumlahBintang; i++){
		for(j=1; j<=i; j++){
			a+="#";
		}
		a+="\n";
	}
	for(i=1; i<=jumlahBintang; i++){
		for(j=i; j<=jumlahBintang; j++){
			a+="#";
		}
		a+="\n";
	}
	return a;
}*/
// Soal no 14 Tampilkan bingtang
function tampilkanBintang(jumlahBintang){
	var a = "";
	for(i=1; i<=jumlahBintang; i++){
		for(j=i; j<=jumlahBintang; j++){
			a+="#";
		}
		a+="\n";
	}
	return a;
}
var bintang = tampilkanBintang(5);
console.log(bintang)

// Soal no 15 kondisional
var jenisKelamin = "L"
var nama = "Sahal"

if(jenisKelamin.toLowerCase()=="p"){
	console.log("ibu " + nama)
}else{
	console.log("bapak " + nama)
}