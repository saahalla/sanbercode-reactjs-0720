// Soal 1
//a
var a = 2;
console.log("LOOPING PERTAMA");
while (a<=20){
	if(a%2==0){
		console.log(a + " - I love coding");
	}
	a++;
}

//b
var a = 20;
console.log("LOOPING KEDUA");
while (a>=2){
	if(a%2==0){
		console.log(a + " - I love coding");
	}
	a--;
}

// Soal 2
for(a=1; a<=20; a++){
	if(a%3==0 && a%2==1){
		console.log(a + " - I Love Coding");
	}else if(a%2==1){
		console.log(a + " - Santai");
	}else if(a%2==0){
		console.log(a + " - Berkualitas");
	}
}

// Soal 3
var a = "";
for(i=1; i<=7; i++){
	for(j=1; j<=i; j++){
		a+="#";
	}
	a+="\n";
}
console.log(a);

// Soal 4
var kalimat="saya sangat senang belajar javascript"

var array = kalimat.split(" ");
console.log(array);

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var buah_sort = daftarBuah.sort();
// var text = buah_sort.join("\n"); 
for(i=0; i<buah_sort.length; i++){
	console.log(buah_sort[i]);
}

