// Soal 1
console.log("//============= Soal 1");
console.log("// Luas Lingkaran");
let luasLingkaran = (r) => {
	return 22/7*r*r;
}
let Luas = luasLingkaran(10);
console.log(Luas);

console.log("// Keliling Lingkaran");
const kelilingLingkaran = (r) => {
	return 2*22/7*r;
}
console.log(kelilingLingkaran(10));

// Soal 2
console.log("\n//============= Soal 2");
let kalimat = "";
function tambahKata(a,b,c,d,e){
	let kalimat = `${a} ${b} ${c} ${d} ${e}`;
	return kalimat;
}
console.log(tambahKata("saya", "adalah", "seorang", "frontend", "developer"));

// Soal 3
/*buatlah class Book yang didalamnya terdapat property name, totalPage, price. lalu buat class baru komik yang extends terhadap buku dan mempunyai property sendiri yaitu isColorful yang isinya true atau false*/
console.log("\n//============= Soal 3");
class Book {
	constructor(name, totalPage, price){
		this._name = name;
		this._totalPage = totalPage;
		this._price =  price;
	}
	get name(){
		return this._name;
	}
	get totalPage(){
		return this._totalPage;
	}
	set totalPage(page){
		this._totalPage = page;
	}
	get price(){
		return this._price;
	}
	tampil(){
		return {nama: this._name, halaman: this._totalPage, harga: this._price};
	}
}
class Komik extends Book {
	constructor(name, totalPage, price, color){
		super(name, totalPage, price);
		this._color = color;
	}
	tampil(){
		return {nama: this._name, halaman: this._totalPage, harga: this._price, isColorful: this._color};
	}
}
let novel = new Book("Novel", "421", 85000);
let komik = new Komik("Tsubasa", "222", 40000, true);
let naruto = new Komik("Naruto", "100", 20000, false);

console.log(novel.tampil());
console.log(komik.tampil());
console.log(naruto.tampil());