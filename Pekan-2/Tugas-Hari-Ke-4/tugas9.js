// Soal 1
console.log("==================== Jawaban 1 ")
const newFunction = function(firstName, lastName) {
	return {
		fullName: function(){
			console.log(`${firstName} ${lastName}`);
		}
	}
}
 
//Driver Code 
newFunction("William", "Imoh").fullName(); 



// Soal 2
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
console.log("\n==================== Jawaban 2 ")
// Jawaban Soal 2 ES 6
const {firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation, spell)



// Soal 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// Jawaban Soal 3 ES 6 
console.log("\n==================== Jawaban 3 ")
const combined = [...west, ...east];

//Driver Code
console.log(combined)

