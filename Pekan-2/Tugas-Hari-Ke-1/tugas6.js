// Soal 1
//ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var objectDaftarPeserta = {
	nama : "Asep",
	"jenis kelamin" : "laki-laki",
	hobi : "baca buku",
	"tahun lahir" : 1992
}
console.log(
	objectDaftarPeserta.nama + "\n" + 
	objectDaftarPeserta["jenis kelamin"] + "\n" + 
	objectDaftarPeserta.hobi + "\n" + 
	objectDaftarPeserta["tahun lahir"]
);

// Soal 2
// anda diberikan data-data buah seperti di bawah ini
// uraikan data tersebut menjadi array of object dan munculkan data pertama
var buah = [
	{
		nama : "strawberry",
		warna : "merah",
		"ada bijinya" : "tidak",
		harga : 9000 
	},
	{
		nama : "jeruk",
		warna : "oranye",
		"ada bijinya" : "ada",
		harga : 8000 
	},
	{
		nama : "Semangka",
		warna : "Hijau & Merah",
		"ada bijinya" : "ada",
		harga : 10000 
	},
	{
		nama : "Pisang",
		warna : "Kuning",
		"ada bijinya" : "tidak",
		harga : 5000 
	}
];
var buahFilter = buah.filter(function(fruit){
	return fruit.nama == "strawberry";
})
console.log(buahFilter);

// Soal 3
// buatlah variable seperti di bawah ini var dataFilm = [];
// buatlah fungsi untuk menambahkan dataFilm tersebut yang itemnya object adalah object dengan property nama, durasi , genre, tahun
var dataFilm = [];
function addDataFilm(nama, durasi, genre, tahun){
	var objectFilm = {
		"nama" : nama,
		"durasi" : durasi,
		"genre" : genre,
		"tahun" : tahun
	}
	dataFilm.push(objectFilm)
}
addDataFilm("500 Days of Summer", "95 min", "Comedy, Drama, Romance", 2009);
addDataFilm("Big Fish", "125 min", "Adventure, Drama, Fantasy", 2003);
console.log(dataFilm);

// Soal 4
//Terdapat sebuah class Animal yang memiliki sebuah constructor name, default property band= 4 dan cold_blooded = false.

// Release 0
/*Buatlah class Animal yang menerima satu parameter constructor berupa name. Secara default class Animal akan memiliki property yaitu legs (jumlah kaki) yang bernilai 4 dan cold_blooded bernilai false.
Gunakan method getter untuk mengakses property di dalam class*/

class Animal {
    constructor(animalname){
    	this.name = animalname;
    	this.legs = 4;
    	this.cold_blooded = false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
/* 
Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4 dari class Animal. class Ape memiliki function yell() yang menampilkan “Auooo” dan class Frog memiliki function jump() yang akan menampilkan “hop hop”.
*/
class Ape extends Animal {
	constructor(animalname){
		super(animalname);
		this.legs = 2;
	}
	yell(){
		return "Auooo";
	}
}
class Frog extends Animal {
	constructor(animalname){
		super(animalname);
	}
	jump(){
		return "hop hop"
	}
}
var sungokong = new Ape("kera sakti");
console.log(sungokong.yell()); // "Auooo"
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)


// Soal 5
/*
Terdapat sebuah class dengan nama Clock yang ditulis seperti penulisan pada function, ubahlah fungsi tersebut menjadi class dan pastikan fungsi tersebut tetap berjalan dengan baik. Jalankan fungsi di terminal/console Anda untuk melihat hasilnya. (tekan tombol Ctrl + C pada terminal untuk menghentikan method clock.start())
*/

class Clock {
    // Code di sini
    constructor({template}){

    	var timer;

    	function render() {
    		var date = new Date();

    		var hours = date.getHours();
    		if (hours < 10) hours = '0' + hours;

    		var mins = date.getMinutes();
    		if (mins < 10) mins = '0' + mins;

    		var secs = date.getSeconds();
    		if (secs < 10) secs = '0' + secs;

    		var output = template
    		.replace('h', hours)
    		.replace('m', mins)
    		.replace('s', secs);

    		console.log(output);
    	}

    	this.stop = function() {
    		clearInterval(timer);
    	};

    	this.start = function() {
    		render();
    		timer = setInterval(render, 1000);
    	};

    }
    
}
var clock = new Clock({template: 'h:m:s'});
clock.start();