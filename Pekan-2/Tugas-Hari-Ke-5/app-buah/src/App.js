import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div style={{marginLeft: "40px", width: "40%", border: "1px solid black", borderRadius: "10px", padding: "15px"}}>
      <h1 style={{textAlign: "center"}}>Form Pembelian Buah</h1>
      <form action="index.html">
        <table>
          <tr>
            <td>
              <label for="nama"><b>Nama Pelanggan</b></label>
            </td>
            <td>
              <input type="text" name="nama" />
            </td>
          </tr>
          <tr>
            <td style={{verticalAlign: "bottom"}}>
              <label required><b>Daftar Item</b></label>
            </td>
            <td>
              <br />
              <input type="checkbox" id="semangka" name="semangka" value="semangka" />
                <label for="semangka">Semangka</label><br />
                <input type="checkbox" id="jeruk" name="jeruk" value="jeruk" />
                <label for="jeruk">Jeruk</label><br />
                <input type="checkbox" id="nanas" name="nanas" value="nanas" />
                <label for="nanas">Nanas</label><br />
                <input type="checkbox" id="salak" name="salak" value="salak" />
                <label for="salak">Salak</label><br />
                <input type="checkbox" id="anggur" name="anggur" value="anggur" />
                <label for="anggur">Anggur</label><br />
            </td>

          </tr>
          <tr>
            <td>
              <br/>
              <input type="submit" value="Kirim" style={{borderRadius: "10px", paddingTop: "4px", paddingBottom: "4px", paddingRight: "10px", paddingLeft: "10px"}} />
            </td>
          </tr>
        </table>
      </form>
    </div>
  );
}

export default App;
