/*// di file promise.js
function readBooksPromise (time, book) {
  console.log(`saya mulai membaca ${book.name}`)
  return new Promise( function (resolve, reject){
    setTimeout(function(){
      let sisaWaktu = time - book.timeSpent
      if(sisaWaktu >= 0 ){
          console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
          resolve(sisaWaktu)
      } else {
          console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
          reject(sisaWaktu)
      }
    }, book.timeSpent)
  })
}
 
module.exports = readBooksPromise*/


var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// Soal 2

function bacaBuku(){
	readBooksPromise(10000, books[0])
	.then(function(bacaLagi){
		// console.log(bacaLagi)
		readBooksPromise(bacaLagi, books[1])
		.then(function(bacaLagi){
			// console.log(bacaLagi)
			readBooksPromise(bacaLagi, books[2])
			.then(function(bacaLagi){
				// console.log(bacaLagi)
			})
			.catch(function(selesai){
				// console.log(selesai)
			})
		})
		.catch(function(selesai){
			// console.log(selesai)
		})
	})
	.catch(function(selesai){
		console.log(selesai)
	})
}

bacaBuku();