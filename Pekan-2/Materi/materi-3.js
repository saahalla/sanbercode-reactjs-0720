// var
console.log("----------- var -------------");
var x = 1;
 
if (x === 1) {
var x = 2;
 
console.log(x);
// expected output: 2
}
 
console.log(x); // 2

// Let
console.log("----------- let ------------");
let y = 1;
 
if (y === 1) {
  let y = 2;
 
  console.log(y);
  // expected output: 2
}
 
console.log(y); // 1 

// const number = 42;
// number = 100; // Uncaught TypeError: Assignment to constant variable.

console.log("------------ Arrow function ------------");
function multiply(a, b = 3) {
  return a * b;
}
 
console.log(multiply(5, 2));
// expected output: 10
 
console.log(multiply(5));
// expected output: 5 

console.log("------------ Template Literals ------------");
const firstName = 'Zell'
const lastName = 'Liew'
const teamName = 'unaffiliated'
 
const theString = `${firstName} ${lastName}, ${teamName}`
 
console.log(theString) // Zell Liew, unaffiliated

console.log("------------ Var Medium ------------");
var foo ='hello1'
var foo ='hello2'
console.log(foo) //=> hello